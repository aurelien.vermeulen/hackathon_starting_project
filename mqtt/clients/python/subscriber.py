import paho.mqtt.client as mqtt
from datetime import datetime

# Configuration
BROKER_ADDRESS = "34.34.149.58"
TOPIC = "topic/protocol-tags"
MQTT_USER = "subscriber"
MQTT_PWD = "smoked-sandbar-aloe"


# Callback when client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected to {0} on {1} with result code {2} ".format(BROKER_ADDRESS, TOPIC, rc))
    client.subscribe(TOPIC)

# Callback when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    now = datetime.now()
    print(f"Received message: {now.isoformat()} {msg.payload.decode()} on topic: {msg.topic}")

# Create MQTT client
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

# Connect to the broker
client.username_pw_set(MQTT_USER, MQTT_PWD)
client.connect(BROKER_ADDRESS, 1883, 60)

# Blocking call that processes network traffic, dispatches callbacks, and handles reconnecting.
client.loop_forever()
