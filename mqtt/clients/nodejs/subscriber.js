const mqtt = require('mqtt');
const topic= 'topic/protocol-tags';
const client = mqtt.connect('mqtt:34.34.149.58', {
    username: 'subscriber',
    password: 'smoked-sandbar-aloe',
});

const LOG_MESSAGES_RECEIVED = true;
const LOG_TURN_DATA = true;
const LOG_STATION = true;

const database = [];
const captorPositions = [
    {
        id: 1,
        x: 3600,
        y: 4200
    },
    {
        id: 2,
        x: 4000,
        y: -1600
    },
    {
        id: 3,
        x: -3550,
        y: -1600
    },
    {
        id: 4,
        x: -3550,
        y: 2600
    }
];
const stationPositions = [
    { // Station grise
        x: 195,
        y: 425
    }
]
const kpi = {
    turn: []
};
const sectionSize = 10;
const minDistanceBetweenPoints = 230;

client.on('connect', () => {
    console.log("I'm connected !");
    client.subscribe(topic, (err) => {
        if(err) {
            console.error(err);
        } else {
            console.log(`increment;date;message;topic`);
        }
    });
});

const computeAverage = (values) => {
    return values.reduce((a, b) => a + b) / values.length;
}

const onNewTurn = () => {
    const previousTurn = kpi.turn.at(-1);
    const turnData = {
        date: database.at(-1).date
    };
    if (!previousTurn) {
        turnData.lapCount = 0;
    } else {
        turnData.lapCount = previousTurn.lapCount + 1;
        turnData.lapTime = Math.abs(new Date(turnData.date) - new Date(previousTurn.date));
    }
    kpi.turn.push(turnData);
    if (LOG_TURN_DATA) {
        console.log(`
        ##############################
            🏁 TURN KPI (${turnData.lapCount}):
                Date: ${turnData.date}
                Time laps: ${turnData.lapTime}
        ##############################
        `);
    }
}

const isTrainRunning = () => {
    const positions = database.filter(data => data.train.position)
        .map(data => data.train.position)
    const nbEntry = positions.length;
    if (nbEntry < sectionSize) {
        return true;
    }
    const previousPosition = positions[nbEntry - sectionSize]
    const currentPosition = positions.at(-1)
    return distance(previousPosition, currentPosition) > minDistanceBetweenPoints;
}

const isNearToStation = () => {
    const lastData = database.at(-1);
    if (!lastData.train.position) {
        return false;
    }
    return stationPositions.some(stationPosition => distance(stationPosition, lastData.train.position) < minDistanceBetweenPoints);
}

const distance = (point1, point2) => {
    let dx = point2.x - point1.x;
    let dy = point2.y - point1.y;
    return Math.sqrt(dx * dx + dy * dy);
}

let approach = false;
const updateTurnData = (departureCaptorId) => {
    const nbEntry = database.length;
    if (nbEntry < sectionSize * 2) {
        return;
    }
    const computeDistance = (section) => computeAverage(section.flatMap(data => data.captors)
        .filter(captor => captor.id === departureCaptorId && !captor.status)
        .map(captor => captor.distance));
    const previousSection = database.slice(nbEntry - sectionSize * 2, nbEntry - sectionSize);
    const currentSection = database.slice(nbEntry - sectionSize, nbEntry - 1);
    // If the train distances itself from the origin.
    if (computeDistance(previousSection) < computeDistance(currentSection)) {
        if (approach) {
            onNewTurn();
        }
        approach = false;
    } else {
        approach = true;
    }
}

const computeTrainPosition = (captors) => {
    const availableCaptors = captors.filter(captor => !captor.status)
    if (availableCaptors.length < 3) {
        return undefined;
    }
    const positions = availableCaptors.slice(0, 3)
        .map(captor => Object.assign({ r: captor.distance }, captorPositions.find(c => c.id === captor.id)))
    return getPositionByTrilateration(positions[0], positions[1], positions[2]);
}

const checkIsTrainRunning = () => {
    return database.length < 10 || database.slice(database.length - sectionSize, database.length - 1)
        .filter(data => data.train.running).length > 5;
}

// receive a message from the subscribed topic
client.on('message',(topic, message) => {
    const rawMessage = `${message}`;
    const captorsData = rawMessage.substring(1, rawMessage.length-1).split('],')
        .map(entry => {
            const values = entry.replace(/[\[\]]/g, '')
                .split(',')
            return {
                id: +values[0],
                distance: +values[1],
                status: +values[2]
            }
        });
    const data = {
        date: (new Date()).toISOString(),
        topic: `${topic}`,
        rawMessage: rawMessage,
        captors: captorsData,
        train: {
            position: computeTrainPosition(captorsData),
            running: isTrainRunning()
        }
    };
    if (LOG_MESSAGES_RECEIVED) {
        console.log('Received :', data)
    }
    database.push(data)
    if (!checkIsTrainRunning()) {
        console.log(`
        ##############################
            ⚠️ Train stopped ⚠️
        ##############################
        `);
        return;
    }
    if (LOG_STATION && isNearToStation()) {
        console.log(`
        ##############################
            ℹ️ Train arrives at the station
        ##############################
        `);
    }
    updateTurnData(1);
});

// error handling
client.on('error',(error) => {
    console.error(error);
    process.exit(1);
});

function getPositionByTrilateration(position1, position2, position3) {
    let xa = position1.x, xb = position2.x, xc = position3.x;
    let ya = position1.y, yb = position2.y, yc = position3.y;
    let ra = position1.r, rb = position2.r, rc = position3.r;
    let d;

    let S = (Math.pow(xc, 2.) - Math.pow(xb, 2.) + Math.pow(yc, 2.) - Math.pow(yb, 2.) + Math.pow(rb, 2.) - Math.pow(rc, 2.)) / 2.0;
    let T = (Math.pow(xa, 2.) - Math.pow(xb, 2.) + Math.pow(ya, 2.) - Math.pow(yb, 2.) + Math.pow(rb, 2.) - Math.pow(ra, 2.)) / 2.0;

    // avoid division by zero
    let y = (T * (xb - xc)) - (S * (xb - xa));// / (((ya - yb) * (xb - xc)) - ((yc - yb) * (xb - xa)));
    d = (((ya - yb) * (xb - xc)) - ((yc - yb) * (xb - xa)));
    if(d){ y = y / d; }

    // avoid division by zero
    let x = (y * (ya - yb)) - T;
    d = (xb - xa);
    if(d){ x = x / d; }

    // with division by zero bug
    //let y = ((T * (xb - xc)) - (S * (xb - xa))) / (((ya - yb) * (xb - xc)) - ((yc - yb) * (xb - xa)));
    //let x = ((y * (ya - yb)) - T) / (xb - xa);

    return { x: x, y: y };
}