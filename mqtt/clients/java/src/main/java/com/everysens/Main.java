package com.everysens;

import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    private static final Logger LOGGER = Logger.getLogger(Main.class.getName());

    private static final String BROKER_ADDRESS = "34.34.149.58";
    private static final String TOPIC = "topic/protocol-tags";
    private static final String USERNAME = "subscriber";
    private static final String PASSWORD = "smoked-sandbar-aloe";

    public static void main(String[] args) {
        if (BROKER_ADDRESS.isBlank() || TOPIC.isBlank()) {
            LOGGER.log(Level.SEVERE, "Missing configuration. Exiting.");
            System.exit(1);
        }

        try {
            MqttClient mqttClient = new MqttClient("tcp://" + BROKER_ADDRESS + ":1883", MqttClient.generateClientId(), new MemoryPersistence());

            MqttConnectOptions options = new MqttConnectOptions();
            options.setUserName(USERNAME);
            options.setPassword(PASSWORD.toCharArray());

            mqttClient.setCallback(new MqttCallback() {

                @Override
                public void connectionLost(Throwable cause) {
                    LOGGER.log(Level.SEVERE, "Connection lost", cause);
                }

                @Override
                public void messageArrived(String topic, MqttMessage message) {
                    LOGGER.log(Level.INFO, "Received message: {0} on topic: {1}", new Object[]{new String(message.getPayload()), topic});
                }

                @Override
                public void deliveryComplete(IMqttDeliveryToken token) {
                    // Not used for subscribers
                }
            });

            mqttClient.connect(options);

            if (mqttClient.isConnected()) {
                LOGGER.log(Level.INFO, "Connected successfully.");
                mqttClient.subscribe(TOPIC);
            } else {
                LOGGER.log(Level.SEVERE, "Failed to connect to MQTT broker.");
            }
        } catch (MqttException e) {
            LOGGER.log(Level.SEVERE, "An MQTT exception occurred", e);
        }
    }
}
