#
# author : vincent
# date : sept 2023
#

from turtle import *
import turtle
import random

screenWidth = 2000      # initial window size
screenHeight = 2000     # initial window size
width = 1400            # width of cartesian coords
height = 1800           # height of cartesian coords
grid = 200              # grid increment

#
# Set world coordinates
#
reset()
setworldcoordinates(-2000,-2000,2000,2000)
speed(0)  # 0 = fastest
tracer()

#
# Setup windows
#
setup(screenWidth, screenHeight)
bgcolor("#C0C0C0")
title("Hackathon Everysens 2023 !")


#
# Show X and Y axes
#
pensize(2)
color('black')
up()
goto(0,height)
down()
goto(0,-height)
up()
goto(-width,0)
down()
goto(width,0)
home()
up()
goto(-50,-50)
write("0,0")


#
# Show grid
#
pensize(1)
color('green')

# Vertical
for x in range(-width, width+1, grid):
    if x != 0:
        up()
        goto(x, height)
        down()
        goto(x, -height)
        up()
        goto(x+8, 0)
        down()
        write("{0},0".format(x))


# Horizontal
for y in range(-height, height+1, grid):
    if y != 0:
        up()
        goto(-width, y)
        down()
        goto(width, y)
        up()
        goto(0+8, y+8)
        down()
        write("0,{0}".format(y))


#
# Show random dots
#
random.seed(5)
for i in range(0, 20):
    x = random.randint(-1000, 1000)
    y = random.randint(-1000, 1000)
    up()
    color('red')
    goto(x, y)
    dot(8, "red")


ready = True
ht()
turtle.mainloop()
